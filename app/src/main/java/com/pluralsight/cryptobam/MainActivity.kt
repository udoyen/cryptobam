package com.pluralsight.cryptobam

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Looper

import android.util.Log
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import com.android.volley.AuthFailureError
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.bumptech.glide.Glide
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.material.floatingactionbutton.FloatingActionButton

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

import java.io.BufferedReader
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.IOException
import java.io.InputStreamReader
import java.util.ArrayList
import java.util.HashMap

class MainActivity : AppCompatActivity() {
    private var recView: RecyclerView? = null
    private var mAdapter: MyCryptoAdapter? = null
    private var mSwipeRefreshLayout: SwipeRefreshLayout? = null
    private var mTracker: Tracker? = null


    ////////////////////////////////////////////////////////////////////////////////////NETWORK RELATED CODE///////////////////////////////////////////////////////////////////////////////////////


    val CRYPTO_URL_PATH = "https://files.coinmarketcap.com/static/img/coins/128x128/%s.png"
    val ENDPOINT_FETCH_CRYPTO_DATA = "https://api.coinmarketcap.com/v1/ticker/?limit=100"
    private var mQueue: RequestQueue? = null
    private val mObjMapper = ObjectMapper()

    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocationRequest: LocationRequest? = null

    private val mLocationCallbacks = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            Log.d(TAG, "onLocationResult() called with: locationResult = [$locationResult]")
            for (location in locationResult.locations) {
                if (location != null) {
                    val lat = location.latitude.toInt()
                    val lng = location.longitude.toInt()
                    mTracker!!.trackLocation(lat, lng)
                }
            }
        }
    }


    //////////////////////////////////////////////////////////////////////////////////////STORAGE CODE///////////////////////////////////////////////////////////////////////////////////////////
    internal var DATA_FILE_NAME = "crypto.data"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bindViews()
        fetchData()
        mTracker = Tracker(this)
        mTracker!!.trackOnCreate()
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        checkLocationPermission()


    }

    override fun onStart() {
        super.onStart()
        mTracker!!.trackOnStart()
    }

    override fun onResume() {
        super.onResume()
        mTracker!!.trackOnResume()
    }

    override fun onPause() {
        super.onPause()
        mTracker!!.trackOnPause()
    }

    override fun onStop() {
        super.onStop()
        mTracker!!.trackOnStop()
    }

    override fun onDestroy() {
        super.onDestroy()
        mTracker!!.trackOnDestroy()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        val id = item.itemId


        return if (id == R.id.action_settings) {
            true
        } else super.onOptionsItemSelected(item)
    }

    private fun bindViews() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        recView = findViewById(R.id.recView)
        mSwipeRefreshLayout = findViewById(R.id.swipeToRefresh)
        mSwipeRefreshLayout!!.setOnRefreshListener { fetchData() }
        mAdapter = MyCryptoAdapter()
        val lm = LinearLayoutManager(this)
//        lm.orientation =
        recView!!.layoutManager = lm
        recView!!.adapter = mAdapter
        recView!!.addItemDecoration(Divider(this))

        setSupportActionBar(toolbar)
        val fab = findViewById<FloatingActionButton>(R.id.fab)
        fab.setOnClickListener { _ -> recView!!.smoothScrollToPosition(0) }
    }

    inner class Divider(context: Context) : RecyclerView.ItemDecoration() {
        private val mDivider: Drawable

        init {
            // TODO FIX
            mDivider = context.resources.getDrawable(R.drawable.list_divider)
        }

        override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
            val left = parent.paddingLeft
            val right = parent.width - parent.paddingRight

            val childCount = parent.childCount
            for (i in 0 until childCount) {
                val child = parent.getChildAt(i)

                val params = child.layoutParams as RecyclerView.LayoutParams

                val top = child.bottom + params.bottomMargin
                val bottom = top + mDivider.intrinsicHeight

                mDivider.setBounds(left, top, right, bottom)
                mDivider.draw(c)
            }
        }
    }

    private class MyCryptoAdapter : RecyclerView.Adapter<MyCryptoAdapter.CoinViewHolder>() {

        internal var mItems: MutableList<CoinModel> = ArrayList()
        val STR_TEMPLATE_NAME = "%s\t\t\t\t\t\t%s"
        val STR_TEMPLATE_PRICE = "%s$\t\t\t\t\t\t24H Volume:\t\t\t%s$"


        override fun onBindViewHolder(holder: CoinViewHolder, position: Int) {
            val model = mItems[position]
            holder.tvNameAndSymbol.setText(String.format(STR_TEMPLATE_NAME, model.name, model.symbol))
            holder.tvPriceAndVolume.setText(String.format(STR_TEMPLATE_PRICE, model.priceUsd, model.volume24H))
            Glide.with(holder.ivIcon).load(model.imageUrl).into(holder.ivIcon)
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoinViewHolder {

            return CoinViewHolder(LayoutInflater.from(parent.context)
                    .inflate(R.layout.list_item, parent, false))
        }

        override fun getItemCount(): Int {
            return mItems.size
        }

        fun setItems(items: List<CoinModel>) {
            this.mItems.clear()
            this.mItems.addAll(items)

        }


        internal inner class CoinViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

            var tvNameAndSymbol: TextView
            var tvPriceAndVolume: TextView
            var ivIcon: ImageView

            init {
                tvNameAndSymbol = itemView.findViewById(R.id.tvNameAndSymbol)
                tvPriceAndVolume = itemView.findViewById(R.id.tvPriceAndVolume)
                ivIcon = itemView.findViewById(R.id.ivIcon)
            }
        }
    }


    private fun showErrorToast(error: String?) {
        Toast.makeText(this, "Error:$error", Toast.LENGTH_SHORT).show()
    }


    private inner class CoinModel(val name: String, val symbol: String, val imageUrl: String, val priceUsd: String, val volume24H: String)

    private fun fetchData() {
        if (mQueue == null)
            mQueue = Volley.newRequestQueue(this)
        // Request a string response from the provided URL.
        val jsonObjReq = JsonArrayRequest(ENDPOINT_FETCH_CRYPTO_DATA,
                { response ->
                    writeDataToInternalStorage(response)
                    val data = parseJSON(response.toString())
                    Log.d(TAG, "data fetched:" + data!!)
                    EntityToModelMapperTask().execute(data)


                },
                { error ->
                    showErrorToast(error.toString())
                    try {
                        val data = readDataFromStorage()
                        val entities = parseJSON(data.toString())
                        EntityToModelMapperTask().execute(entities)

                    } catch (e: JSONException) {
                        e.printStackTrace()
                    }


                })
        // Add the request to the RequestQueue.
        mQueue!!.add(jsonObjReq)
    }


    fun parseJSON(jsonStr: String): ArrayList<CryptoCoinEntity>? {
        var data: ArrayList<CryptoCoinEntity>? = null

        try {
            data = mObjMapper.readValue<ArrayList<CryptoCoinEntity>>(jsonStr, object : TypeReference<ArrayList<CryptoCoinEntity>>() {

            })
        } catch (e: Exception) {
            showErrorToast(e.message)
            e.printStackTrace()
        }

        return data
    }


    private inner class EntityToModelMapperTask : AsyncTask<List<CryptoCoinEntity>, Void, List<CoinModel>>() {
        override fun doInBackground(vararg data: List<CryptoCoinEntity>): List<CoinModel> {
            val listData = ArrayList<CoinModel>()
            var entity: CryptoCoinEntity
            for (i in 0 until data[0].size) {
                entity = data[0][i]
                listData.add(CoinModel(entity.name, entity.symbol, String.format(CRYPTO_URL_PATH, entity.id), entity.priceUsd, entity.get24hVolumeUsd()))
            }
            return listData
        }

        override fun onPostExecute(data: List<CoinModel>) {
            mAdapter!!.setItems(data)
            mSwipeRefreshLayout!!.isRefreshing = false
        }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////TRACKING CODE/////////////////////////////////////////////////////////////////////////////////////


    internal class Tracker(private val con: Context) {
        private val TRACKING_URL = "https://www.google.com"
        private val mQueue: RequestQueue
        private val mOsVersion: String

        init {
            mOsVersion = Build.VERSION.RELEASE
            mQueue = Volley.newRequestQueue(con)

        }

        private fun generateTrackingStringRequest(eventName: String): StringRequest {
            return object : StringRequest(Request.Method.POST, TRACKING_URL,
                    { response ->
                        Log.d(TAG, "onResponse() called with: response = [$response]")

                    },
                    { error -> Log.d(TAG, "onErrorResponse() called with: error = [$error]") }) {
                @Throws(AuthFailureError::class)
                override fun getBody(): ByteArray {
                    val params = HashMap<String, String>()
                    params["eventName"] = eventName
                    params["osVersion"] = mOsVersion
                    return JSONObject(params as Map<*, *>).toString().toByteArray()
                }
            }
        }

        fun trackOnCreate() {

            mQueue.add(generateTrackingStringRequest("create"))
        }

        fun trackOnDestroy() {

            mQueue.add(generateTrackingStringRequest("destroy"))

        }

        fun trackOnStart() {

            mQueue.add(generateTrackingStringRequest("start"))

        }

        fun trackOnResume() {

            mQueue.add(generateTrackingStringRequest("resume"))

        }

        fun trackOnPause() {

            mQueue.add(generateTrackingStringRequest("pause"))

        }

        fun trackOnStop() {

            mQueue.add(generateTrackingStringRequest("stop"))

        }

        fun trackLocation(lat: Int, lng: Int) {
            mQueue.add(generateTrackingStringRequest("location\t$lat-$lng"))

        }

        companion object {

            private val TAG = Tracker::class.java.simpleName
        }
    }

    private fun checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(this, "Please give me location permissions", Toast.LENGTH_SHORT).show()
            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                        PERMISSION_REQUEST_LOCATION)
            }
        }
    }

    @Synchronized
    protected fun buildGoogleApiClient() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(object : GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(bundle: Bundle?) {
                        Log.d(TAG, "onConnected() called with: bundle = [$bundle]")
                        mLocationRequest = LocationRequest()
                        mLocationRequest!!.interval = 10000 // two minute interval
                        mLocationRequest!!.fastestInterval = 10000
                        mLocationRequest!!.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
                        if (ContextCompat.checkSelfPermission(this@MainActivity,
                                        Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                            mFusedLocationClient!!.requestLocationUpdates(mLocationRequest, mLocationCallbacks, Looper.myLooper())
                        }
                    }

                    override fun onConnectionSuspended(i: Int) {
                        Log.d(TAG, "onConnectionSuspended() called with: i = [$i]")
                    }
                })
                .addOnConnectionFailedListener { connectionResult -> Log.d(TAG, "onConnectionFailed() called with: connectionResult = [$connectionResult]") }
                .addApi(LocationServices.API)
                .build()
        mGoogleApiClient!!.connect()
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_LOCATION -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(this,
                                    Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient()
                        }
                    }

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    ActivityCompat.requestPermissions(this,
                            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                            PERMISSION_REQUEST_LOCATION)
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }

    private fun writeDataToInternalStorage(data: JSONArray) {
        var fos: FileOutputStream? = null
        try {
            fos = openFileOutput(DATA_FILE_NAME, Context.MODE_PRIVATE)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        try {
            fos!!.write(data.toString().toByteArray())
            fos.close()

        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    @Throws(JSONException::class)
    private fun readDataFromStorage(): JSONArray {
        var fis: FileInputStream? = null
        try {
            fis = openFileInput(DATA_FILE_NAME)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }

        val isr = InputStreamReader(fis!!)
        val bufferedReader = BufferedReader(isr)
        val sb = StringBuilder()
        var line: String? = null
        try {
            while ((bufferedReader.readLine()) != null) {
                sb.append(line)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        return JSONArray(sb.toString())
    }

    companion object {

        private val TAG = MainActivity::class.java.simpleName


        ////////////////////////////////////////////////////////////////////////////////////LOCATION RELATED CODE/////////////////////////////////////////////////////////////////////////////////////
        private val PERMISSION_REQUEST_LOCATION = 1234
    }
}
